import base64
import http.client
import json
import os
import random

import msgpack
import json

from bottle import abort, get, post, request, route, static_file
from kajiki_view import Util, kajiki_view


# 0 = RED, 1 = GREEN, 2 = LOADING
def change_state(state):
    if 'NO_BOX' in os.environ:
        return
    connection = http.client.HTTPConnection("0.0.0.0:8080")
    connection.request("GET", '/change_state/{}'.format(state))
    connection.close()


@post('/status/<value>')
def post_status(value=0):
    status = request.forms.get("status")
    open("status", 'w').close()
    with open("status", 'w') as status:
        status.write(value)
    return dict(data=[{"status", value}])


def get_catalog_details(id=0):
    for item in get_catalog():
        if item['id'] == int(id):
            return item

    raise Exception('Not found')


def get_catalog():
    with open('./data/catalog.json') as catalog_file:
        catalog = json.loads(catalog_file.read())
        for i, c in enumerate(catalog):
            cid = str(c['id'])
            catalog[i] = {
                **c,
                'humanID': '{}-{}-{}'.format(cid[:3], cid[3:6], cid[6:]),
            }
        return catalog


### BEGIN COPY-AND-PASTE
def enpack(data):
    return base64.b32encode(json.dumps(data).encode('utf-8')).decode('utf-8')

def depack(msg):
    return json.loads(base64.b32decode(msg).decode('utf-8'))

TRANS_TABLE = str.maketrans("0123456789abcdef", "2189779645045313")
def permute_token(token):
    from hashlib import blake2b

    m = blake2b(digest_size=5)
    m.update(b"we drop tables")
    m.update(str.encode(token))
    m.hexdigest()
    return m.hexdigest()[:9].translate(TRANS_TABLE)

def format_code(code):
    return '{}-{}-{}'.format(code[:3], code[3:6], code[6:])

class FileMeta:
    def __init__(self, **kwargs):
        for at in ['dest', 'token', 'date', 'filename', 'mime']:
            arg = kwargs.get(at, None)
            setattr(self, at, kwargs[at])

    @staticmethod
    def load(meta):
        dest, date, metadata = meta.split('__')
        token, filename, mime = depack(metadata)
        return FileMeta(dest=dest, date=date, token=token, filename=filename, mime=mime)

    def store(self):
        return '__'.join([
            self.dest,
            self.date,
            enpack([
                self.token,
                self.filename,
                self.mime,
            ]).decode(),
        ])
### END COPY-AND-PASTE

@post('/upload')
@kajiki_view('upload_complete')
def post_upload():
    change_state(2)
    upload = request.files.get('file')
    save_path = './data/packages'

    try:
        meta = FileMeta.load(upload.filename)
        central_token = permute_token(meta.token)
        meta.token = permute_token(central_token)
        filename = meta.store()
    except Exception  as e:
        print('upload processing error', e)
        return {
            'page': 'upload',
            'flash': {
                'content': 'Could not upload!',
                'cls': 'danger',
            },
            'token': "error",
        }


    print('confirm', central_token, 'download', meta.token)

    file_path = '{}/{}'.format(save_path, filename)

    if not os.path.exists(file_path):
        upload.save(file_path)

    return {
        'page': 'upload',
        'flash': {
            'content': 'Upload complete!',
            'cls': 'ok',
        },
        'token': format_code(central_token),
    }


@route('/')
@kajiki_view('index')
def root():
    return {'page': 'index', 'catalog_entries': get_catalog()}


@get('/catalog/<id>')
@kajiki_view('catalog_view')
def catalog_view(id):
    return {'page': 'catalog', 'entry': get_catalog_details(id)}


@get('/upload')
@kajiki_view('upload')
def get_upload():
    return {'page': 'upload'}


@get('/download')
@kajiki_view('download')
def get_download():
    return {'page': 'download'}


@get('/download/<id>')
def get_data(id):
    if not os.path.exists('data/packages'):
        abort(400, 'Download not found.')
    for f in os.listdir('data/packages'):
        meta = FileMeta.load(f)
        print(meta.token, meta.filename, meta.dest)
        if meta.token == ''.join(id.split('-')):
            if meta.dest == Util.box_location():
                # download the file without the metadata
                return static_file(
                    f, root='data/packages', mimetype=meta.mime, download=meta.filename)
            else:
                # download the file with all the metadata
                return static_file(f, root='data/packages', download=f)

    abort(400, 'Download not found.')


@route('/data/<filename:path>')
def data(filename):
    return static_file(filename, root='data')

@route('/assets/<filename:path>')
def assets(filename):
    return static('assets/{}'.format(filename))


def static(filename):
    return static_file(filename, root='./')
