import sys

from bottle import run

import app

if len(sys.argv) != 2:
    print('Invalid params. Usage: python __init__.py <location>')
    sys.exit(1)

run(host='0.0.0.0', port=8040)
