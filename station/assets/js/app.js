const dl = $('#download');
const cid = $('#content-id');
cid.on('input', () => {
  const v = cid.val();
  let newStr = '';
  const regex = new RegExp('^[0-9\\-]+$');
  for (const char of v.split('')) {
    if (regex.test(char)) {
      newStr += char;
    }
  }
  dl.attr('href', `/download/${newStr}`);
  cid.val(newStr);
});
