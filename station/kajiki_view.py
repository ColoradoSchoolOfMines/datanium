#! /usr/bin/env python3
"""
Defines the Kajiki View Decorator
"""

import functools
import sys
from urllib.parse import quote

from kajiki import FileLoader, XMLTemplate

loader = FileLoader('templates')
loader.extension_map['xhtml'] = XMLTemplate


class Util:
    def app_name():
        return 'Datanium'

    def box_location():
        return sys.argv[1]

    def get_sms_url(body):
        return 'sms:+17177143767?body={}'.format(quote(body))


def kajiki_view(template_name):
    """
    Defines the kajiki_view decorator

    Used code example from here:
    https://buxty.com/b/2013/12/jinja2-templates-and-bottle/ but customized for
    kajiki instead
    """

    def decorator(view_func):
        @functools.wraps(view_func)
        def wrapper(*args, **kwargs):
            response = view_func(*args, **kwargs)

            if isinstance(response, dict):
                # If the decorated function returns a dictionary, throw that to
                # the template
                Template = loader.load('{}.xhtml'.format(template_name))

                t = Template({
                    **response,
                    'util': Util,
                })
                return t.render()
            else:
                return response

        return wrapper

    return decorator
