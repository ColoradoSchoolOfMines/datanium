#!/usr/bin/env python3

import smbus
from bottle import Bottle, template, static_file, run

bus = smbus.SMBus(1)

address = 0x04

app = Bottle()


@app.route('/')
def index():
    return static_file('html/index.html', root='.')
    # return template('templates/index.tpl')


@app.route('/state')
def state():
    return template('templates/index.tpl')


@app.route('/change_state/<state>')
def change_state(state):
    # state = request.forms.get('state')
    bus.write_byte(address, int(state))
    return template('templates/index.tpl')


run(app, host='0.0.0.0', port=8080)
