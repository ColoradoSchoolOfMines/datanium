#include <Wire.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#include <FastLED.h>

// global defines for program

// pin that the led strip control pin is connected to
#define LED_PIN     10
// number of LEDS on the strip to make use of
#define NUM_LEDS    11
// other LED strip defines
#define BRIGHTNESS  64
#define LED_TYPE    WS2812B

// array allowing addressing the LEDs
CRGB leds[NUM_LEDS];

// i2c setup
#define SLAVE_ADDRESS 0x04

// servo setup
Servo mailbox_servo;

// setup LCD
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
int text_state = 0;

// enum represeting the state of the transaction
enum LED_STATE
{
    OUTDATED = 0,
    UP_TO_DATE = 1,
    UPDATING = 2
};

// defined a global int to store the state
// default value is UP_TO_DATE, aka solid green
volatile int state = OUTDATED;


#define UPDATES_PER_SECOND 100

void on_recieve(int byte_count)
{
  Serial.println("i2c message recieved");
  int new_state = Wire.read();
  if ((new_state >= 0) && (new_state <= 3)) {
    state = new_state;
    Serial.print("new_state ");
    Serial.print(new_state);
    Serial.println(" being assigned to state.");
  }
  else {
    Serial.print("  Invalid state ");
    Serial.print(new_state);
    Serial.println(" sent but not assiged.");
  }
  
}

void setup()
{ 
  mailbox_servo.attach(9);
  
  FastLED.addLeds<WS2811, LED_PIN>(leds, NUM_LEDS);

  Serial.begin(115200);

  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(on_recieve);
  
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  
  Serial.println("Setup Complete\n");
}

void loop()
{
    if (state == OUTDATED) {       
        text_state = 0; 
        for (int i = 0; i < NUM_LEDS; i++) {
            leds[i] = CRGB::Green;
        }
        FastLED.show();
        mailbox_servo.write(45);
        lcd.setCursor(0, 0);
        lcd.clear();
        lcd.print("Data is outdated");
        delay(200);
    }
    else if (state == UP_TO_DATE) {
        text_state = 0;
        for (int i = 0; i < NUM_LEDS; i++) {
            leds[i] = CRGB::Red;
        }
        FastLED.show(); 
        mailbox_servo.write(170);
        lcd.setCursor(0, 0);
        lcd.clear();
        lcd.print("Data is up to");
        lcd.setCursor(0, 1);
        lcd.print("date");
        delay(200);
    }
    else if (state == UPDATING) {
        mailbox_servo.write(45);
        
        if (text_state == 0) {
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("Uploading");
          text_state = text_state + 1;
        }
        else if (text_state < 8) {
            lcd.setCursor(8 + text_state, 0);
            lcd.print(".");
            text_state++;
        }
        else {
            state = UP_TO_DATE;
            text_state = 0;
        }
        
        for (int i = 0; i < NUM_LEDS; i++) {
            leds[i] = CRGB::Yellow;
            for (int j = 0; j < NUM_LEDS; j++) {
              if (j != i) {
                leds[j] = CRGB::Black;
              }
            }
            //int val = map(i, 0, NUM_LEDS, 0, 180);
            //mailbox_servo.write(val/2);
            FastLED.show(); 
            FastLED.delay(20);
        }

        for (int i = NUM_LEDS - 1; i >= 0; i--) {
            leds[i] = CRGB::Yellow;
            for (int j = 0; j < NUM_LEDS; j++) {
              if (j != i) {
                leds[j] = CRGB::Black;
              }
            }
            //int val = map(i, 0, NUM_LEDS, 0, 180);
            //mailbox_servo.write(val/2);
            FastLED.show(); 
            FastLED.delay(20);
        }
        
        
    }
}
