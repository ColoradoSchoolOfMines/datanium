import base64
import datetime
import os
from urllib.parse import urljoin

import msgpack
import json
import requests
from google.cloud import datastore
from google.cloud.datastore.entity import Entity
from google.cloud.datastore.key import Key

import bottle
from bottle import post, request, route, get

TWILIO_ACCOUNT_SID = os.environ['TWILIO_ACCOUNT_SID']
TWILIO_AUTH_TOKEN = os.environ['TWILIO_AUTH_TOKEN']
TWILIO_NUMBER = os.environ['TWILIO_NUMBER']
GITLAB_REPO = 'https://gitlab.com/ColoradoSchoolOfMines/datanium/raw/master'
GCP_PROJECT = 'dropdata-tables'
ds_client = datastore.Client()


@route('/reload_catalog')
def get_catalog():
    global CATALOG
    CATALOG = requests.get(f'{GITLAB_REPO}/station/data/catalog.json').json()


get_catalog()

@route('/')
def index():
    return "Welcome to datanium central."

### BEGIN COPY-AND-PASTE
def enpack(data):
    return base64.b32encode(json.dumps(data).encode('utf-8')).decode('utf-8')

def depack(msg):
    return json.loads(base64.b32decode(msg).decode('utf-8'))

TRANS_TABLE = str.maketrans("0123456789abcdef", "2189779645045313")
def permute_token(token):
    from hashlib import blake2b

    m = blake2b(digest_size=5)
    m.update(b"we drop tables")
    m.update(str.encode(token))
    m.hexdigest()
    return m.hexdigest()[:9].translate(TRANS_TABLE)

def format_code(code):
    return '{}-{}-{}'.format(code[:3], code[3:6], code[6:])

class FileMeta:
    def __init__(self, **kwargs):
        for at in ['dest', 'token', 'date', 'filename', 'mime']:
            arg = kwargs.get(at, None)
            setattr(self, at, kwargs[at])

    @staticmethod
    def load(meta):
        dest, date, metadata = meta.split('__')
        token, filename, mime = depack(metadata)
        return FileMeta(dest=dest, date=date, token=token, filename=filename, mime=mime)

    def store(self):
        return '__'.join([
            self.dest,
            self.date,
            enpack([
                self.token,
                self.filename,
                self.mime,
            ]).decode(),
        ])
### END COPY-AND-PASTE


class Path:
    def __init__(self, path, edge):
        if path:
            self.edges = [edge, *path.edges]
            self.price = edge['price'] + path.price
        else:
            self.edges = [edge]
            self.price = edge['price']

    def __lt__(self, other):
        self.price < other.price

    def __str__(self):
        from itertools import chain
        first = self.edges[0]
        return " -> ".join(
            map(node_name,
                chain((first['from'], ), (edge['to'] for edge in self.edges))))


def best_path_to(dest: Key, startfn):
    from heapq import heappush, heappop, heapify
    from collections import defaultdict
    feeds = defaultdict(list)

    query = ds_client.query(kind='Edge')
    for edge in query.fetch():
        feeds[edge['to']].append(edge)

    queue = [Path(None, edge) for edge in feeds[dest]]
    heapify(queue)
    visited = set([dest])

    while True:
        try:
            path = heappop(queue)
            node = path.edges[0]['from']
            if node in visited:  # better path already found
                continue
            if startfn(node):  # node is valid starting point
                return path
            visited.add(node)  # best path to node now found
            for edge in feeds[node]:  # look at all incoming paths next
                heappush(queue, Path(path, edge))
        except IndexError:
            return None


def node_name(key):
    return ds_client.get(key)['name']


def get_next_edge(dest, startfn):
    path = best_path_to(dest, startfn)
    if not path:
        return None
    print(str(path))
    return path.edges[0]


def send_instructions(edge, token):
    from twilio import rest

    person = ds_client.get(edge['by'])
    start = ds_client.get(edge['from'])['name']
    dest = ds_client.get(edge['to'])['name']

    print(f'Instructing {person["name"]} to transfer \"{token}\" from {start} to {dest}.')

    tw_client = rest.Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    tw_client.messages.create(
        to=person['phone'],
        from_=TWILIO_NUMBER,
        body='\n'.join([
            f'Data is ready for you in {start}!',
            f'Download with code {format_code(token)} and deliver it to {dest}.',
            f'Upon delivery, you will be payed ₮{edge["price"]:,}.',
        ]))

def make_it_so(media, person):
    import random
    dest = person['deliver']
    dest_name = node_name(dest)

    print(f'{person["name"]} wants {media["name"]} delivered to {dest_name}')

    edge = get_next_edge(
        dest, lambda n: ds_client.get(n).get('uri', None) is not None)

    if edge is None:
        return False

    upload_uri = ds_client.get(edge['from'])['uri']
    upload_uri = urljoin(upload_uri, '/upload')

    station_token = ''.join(random.choices("0123456789", k=9))
    central_token = permute_token(station_token)
    station_token_next = permute_token(central_token)
    central_token_next = permute_token(station_token_next)
    delivery = Entity(key=Key('Delivery', station_token, project=GCP_PROJECT))
    delivery['token'] = central_token_next
    delivery['source'] = edge['from']
    delivery['buyer'] = person.key
    delivery['dest'] = dest
    delivery['current'] = edge
    delivery['price'] = 0
    ds_client.put(delivery)

    # Upload file to the entry node
    print(f'Uploading {station_token} (confirm {central_token_next}, download {station_token_next}) to to {upload_uri}.')
    media_stream = requests.get(media['uri'], stream=True)
    filename = FileMeta(
        dest=dest_name,
        date=datetime.datetime.today().strftime('%Y-%m-%d'),
        token=station_token,
        filename=media['uri'].split('/')[-1],
        mime=media_stream.headers['Content-Type'],
    ).store()
    requests.post(
        upload_uri,
        files={ 'file': (filename, media_stream.content) },
        data={})
    send_instructions(edge, station_token_next)

    return True

def media_from_id(ind):
    for media in CATALOG:
        if media['id'] == ind:
            return media
    return None

def node_from_city(city):
    query = ds_client.query(kind='Box')
    query.add_filter('name', '=', city)
    return next(iter(query.fetch()), None)


def person_from_number(number):
    query = ds_client.query(kind='Person')
    query.add_filter('phone', '=', number)
    return next(iter(query.fetch()), None)


def any_person():
    query = ds_client.query(kind='Person')
    return next(iter(query.fetch()), None)


class Dialog:
    def __init__(self, req):
        self.input = req['queryResult']
        self.sess = req['session']
        self.odir = req['originalDetectIntentRequest']
        self.output = {'outputContexts': []}

    @property
    def action(self):
        return self.input['action']

    @property
    def params(self):
        return self.input['parameters']

    def param(self, name, *args):
        return self.params.get(name, *args)

    @property
    def person(self):
        if self.odir.get('source') == 'twilio':
            twilio_data = self.odir['payload']['data']
            return person_from_number(twilio_data['From'])

    @property
    def default_contexts(self):
        return self.input.get('outputContexts', [])

    @property
    def ready_contexts(self):
        return self.output['outputContexts']

    def message(self, msg):
        self.output['fulfillmentText'] = msg

    def context(self, name):
        name = f'{self.sess}/contexts/{name}'
        get = lambda li: next((x for x in li if x['name'] == name), None)

        ready = get(self.ready_contexts)
        if not ready:
            ready = get(self.default_contexts)
            self.ready_contexts.append(ready)
        return ready

    def cancel_context(self, name):
        self.context(name)['lifespanCount'] = 0

    def cancel_all(self):
        self.output['outputContexts'] = self.default_contexts
        for ctx in self.output['outputContexts']:
            ctx['lifespanCount'] = 0

def prepurchase(dialog, media, person):
    price = media['price']
    balance = person['wallet']

    if price <= balance:
        dialog.message(f'Found {media["name"]} for ₮{price:,}.\nYou have ₮{balance:,} in your account.\nContinue?')
    else:
        dialog.cancel_context('buy-direct-followup')
        dialog.message(f'Could not purchase {media["name"]} for ₮{price:,}. You only have ₮{balance:,} in your account.')

def handle_dialog(dialog):
    person = dialog.person or any_person()  # JUST FOR TESTING, LOL
    action = dialog.action

    if action == 'pre-purchase-id':
        try:
            media = media_from_id(int(dialog.param('id')))
        except ValueError:
            media = None

        if not media:
            dialog.cancel_context('buy-direct-followup')
            dialog.message('Unknown product in catalog.')
            return

        prepurchase(dialog, media, person)

    elif action == 'general-code':
        central_token = dialog.param('id')
        try:
            code = int(central_token)
            media = media_from_id(code)
            if media:
                prepurchase(dialog, media, person)
                return
        except ValueError:
            pass

        dialog.cancel_context('buy-direct-followup')

        query = ds_client.query(kind='Delivery')
        query.add_filter('token', '=', central_token)
        delivery = next(iter(query.fetch()), None)

        if delivery is None:
            dialog.message(f'Unknown code. Are you sure this delivery is correct?')
            return

        dest = delivery['dest']
        current = delivery['current']

        price = current['price']
        courier = ds_client.get(current['by'])
        courier['wallet'] += price

        at = current['to']
        edge = get_next_edge(dest, lambda n: n == at)
        delivery['price'] += price
        delivery['current'] = edge
        station_token = permute_token(central_token)
        central_token_next = permute_token(station_token)
        delivery['token'] = central_token_next

        print(f'download={station_token} next_confirm={central_token_next}')

        dialog.message(f'Thanks for the delivery! We have depositied ₮{price:,} into your account.')
        ds_client.put_multi([courier, delivery])

        if edge:
            send_instructions(edge, station_token)
        else:
            from twilio import rest

            buyer = ds_client.get(delivery['buyer'])
            tw_client = rest.Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
            dname = node_name(dest)
            if at == dest:
                tw_client.messages.create(
                    to=buyer['phone'],
                    from_=TWILIO_NUMBER,
                    body='\n'.join([
                        f'Data is ready for you in {dname}!',
                        f'Download with code {format_code(station_token)}.',
                    ]))
            else:
                tw_client.messages.create(
                    to=buyer['phone'],
                    from_=TWILIO_NUMBER,
                    body='\n'.join([
                        f'Could not deliver data to {dname}.',
                        f'Your purchase will be refunded.',
                    ]))
                    # TODO: actually refund

    elif action == 'purchase-id':
        media = media_from_id(int(dialog.param('id')))

        if make_it_so(media, person):
            person['wallet'] -= media['price']
            ds_client.put(person)
            dialog.message(f'Purchase of {media["name"]} complete! You will be notified when your data is available.')
        else:
            dialog.message(f'Could not purchase, delivery impossible.')

    elif action == 'day-offer':
        node_from = node_from_city(dialog.param('from'))
        node_to = node_from_city(dialog.param('to'))
        price = dialog.param('price')
        currency = price['currency']
        price = price['amount']

        if not node_from or not node_to:
            dialog.message(f'Unknown box location.')
            dialog.cancel_all()
            return

        if currency == 'USD':
            price *= 2545
        elif currency != 'MNT':
            dialog.message(f'Unknown currency {currency}.')
            dialog.cancel_all()
            return

        key = ds_client.allocate_ids(Key('Edge', project=GCP_PROJECT), 1)[0]
        edge = Entity(key=key)
        edge['from'] = node_from.key
        edge['to'] = node_to.key
        edge['price'] = price
        edge['by'] = person.key
        ds_client.put(edge)

        dialog.message(f'Thank you! You will be notified soon if data needs to be transported.')
    else:
        dialog.message(f'Unknown action: \"{action}\".')

@get('/state.json')
def statejson():
    people_query = ds_client.query(kind='Person')
    people = {x.id: { 'name': x['name'], 'number': x['phone'], 'balance': x['wallet'] } for x in people_query.fetch()}

    nodes_query = ds_client.query(kind='Box')
    nodes = {x.id: { 'name': x['name'], 'lat': x['location'].latitude, 'lon': x['location'].longitude } for x in nodes_query.fetch()}

    edges_query = ds_client.query(kind='Edge')
    edges = [{ 'person': x['by'].id, 'from': x['from'].id, 'to': x['to'].id, 'price': x['price'] } for x in edges_query.fetch()]

    deliveries_query = ds_client.query(kind='Delivery')
    deliveries = [{
        'buyer': x['buyer'].id,
        'source': x['source'].id,
        'dest': x['dest'].id,
        'current': {
            'person': x['current']['by'].id,
            'from': x['current']['from'].id,
            'to': x['current']['to'].id,
        },
        'price': x['price']
    } for x in deliveries_query.fetch()]

    return {
        'people': people,
        'nodes': nodes,
        'edges': edges,
        'deliveries': deliveries,
    }


@post('/dialog')
def receive_dialog():
    dialog = Dialog(request.json)
    # print(json.dumps(dialog.input))

    try:
        handle_dialog(dialog)
    except Exception:
        import traceback
        traceback.print_exc()

        dialog.cancel_all()
        dialog.message('An internal error occurred.')

    return dialog.output


if __name__ == '__main__':
    bottle.run(host='0.0.0.0', port=8080, debug=True)

app = bottle.default_app()
